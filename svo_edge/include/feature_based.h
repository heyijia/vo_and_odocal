#ifndef FEATURE_BASED_H
#define FEATURE_BASED_H

#include <svo/global.h>
#include <svo/feature_detection.h>

namespace svo {

class  Feature_based_track
{

public:
   Feature_based_track( feature_detection::DetectorPtr feature_detector)
   {
     fts_detector_ = feature_detector;
   }
   bool track(const FramePtr old_frame, const FramePtr new_frame);

private:
  feature_detection::DetectorPtr fts_detector_;
};


}
#endif // FEATURE_BASED_H
