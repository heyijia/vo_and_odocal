#ifndef CLIENT_H
#define CLIENT_H
#include "opencv2/core/core.hpp"
#include <sys/socket.h>

#include <sophus/se3.h>
#include <eigen3/Eigen/Core>
#include <vector>

#include <mutex>
namespace SVO {

class SocketClient
{

public:
  SocketClient(std::string hostname, int port);
  int initSock(const char *ip, int port);

  int SendPose(Sophus::SE3 T_world_cam, double t);

  std::mutex mMutexCloseFlag;
  bool close_socket_flag;
  bool CheckClose()
  {
         std::unique_lock< std::mutex> lock(mMutexCloseFlag);
         return close_socket_flag;
  }

  void CloseSocket()
  {
       std::unique_lock< std::mutex> lock(mMutexCloseFlag);
       close_socket_flag = true;
  }

private:
  std::string hostname_;
  int port_;
  int sockfd_;
  int send_what_;

  //cv::Mat T_world_from_vision_;
  Sophus::SE3 T_world_from_vision_;
};

}


#endif // CLIENT_H
