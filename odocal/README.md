#vo2

##　odom管理策略
odom归frame管理，每个frame里带着odom指针，frame初始化的时候，获取odom的姿态存到frame的$T_{or}$里。

odom还是用SE3替代吧。calcam那里好调用。
## note
1. odom和vo融合的时候别相邻两帧就融合一次，多运动几帧再融合。(同理，标定外参的时候也一样)
2. g2o创建一个edge或者vertex的时候，一定要把read(),write那些函数实现一下，不然编译不过去的。
3. g2o只有debuge版本貌似安装好了，可以在程序里用